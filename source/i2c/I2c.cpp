/*
 * I2c.cpp
 *
 *  Created on: Nov 5, 2018
 *      Author: josop
 */

#include <i2c/I2c.h>
#include <stdio.h>

I2c::I2c() {
	/* Gets the default configuration for master. */
	I2C_MasterGetDefaultConfig(&m_i2cMasterConfig);
	/* Inititializes the I2C master. */
	I2C_MasterInit(I2C0, &m_i2cMasterConfig, CLOCK_GetFreq(I2C0_CLK_SRC));
}

I2c::~I2c() {
	// TODO Auto-generated destructor stub
}


void I2c::printLastStatus() {
	printf("Status of last operation is : ");

	switch(m_lastOpStatus){
	case kStatus_Success:
		printf("Successfully complete the data transmission.\n");
		break;
	case kStatus_I2C_Busy:
		printf("Previous transmission still not finished.\n");
		break;
	case kStatus_I2C_Timeout:
		printf("Transfer error, wait signal timeout.\n");
		break;
	case kStatus_I2C_ArbitrationLost:
		printf("Transfer error, arbitration lost.\n");
		break;
	case kStatus_I2C_Nak:
		printf("Transfer error, receive NAK during transfer.\n");
		break;
	default:
		printf("unknown status");
	}
}


bool I2c::write(uint8_t address, uint8_t* pData, size_t length) {

	i2c_master_transfer_t i2cMasterXfer;

	i2cMasterXfer.slaveAddress = address;
	i2cMasterXfer.direction = kI2C_Write;
	i2cMasterXfer.subaddress = 0;
	i2cMasterXfer.subaddressSize = 0;
	i2cMasterXfer.data = pData;
	i2cMasterXfer.dataSize = length;
	i2cMasterXfer.flags = kI2C_TransferDefaultFlag;

	m_lastOpStatus = I2C_MasterTransferBlocking(I2C0, &i2cMasterXfer);

	if(m_lastOpStatus == kStatus_Success)
		return true;
	return false;
}


bool I2c::read(uint8_t address, uint8_t* pData, size_t length) {
	i2c_master_transfer_t i2cMasterXfer;

	i2cMasterXfer.slaveAddress = address;
	i2cMasterXfer.direction = kI2C_Read;
	i2cMasterXfer.subaddress = 0;
	i2cMasterXfer.subaddressSize = 0;
	i2cMasterXfer.data = pData;
	i2cMasterXfer.dataSize = length;
	i2cMasterXfer.flags = kI2C_TransferDefaultFlag;

	m_lastOpStatus = I2C_MasterTransferBlocking(I2C0, &i2cMasterXfer);

	if(m_lastOpStatus == kStatus_Success)
			return true;
	return false;
}

bool I2c::readRegs(uint8_t address, uint8_t regAddr, uint8_t* data, size_t length) {
	//uint32_t status;

	i2c_master_transfer_t i2cMasterXfer;

	i2cMasterXfer.slaveAddress = address;
	i2cMasterXfer.direction = kI2C_Read;
	i2cMasterXfer.subaddress = regAddr;
	i2cMasterXfer.subaddressSize = 1;
	i2cMasterXfer.data = data;
	i2cMasterXfer.dataSize = length;
	i2cMasterXfer.flags = kI2C_TransferDefaultFlag;

	m_lastOpStatus = I2C_MasterTransferBlocking(I2C0, &i2cMasterXfer);

	if (m_lastOpStatus == kStatus_Success)
		return true;
	return false;

//	/* Sends a start and a slave address. */
//	I2C_MasterStart(I2C0, addr, kI2C_Write);
//	/* Waits for the sent out address. */
//	while(!((status = I2C_MasterGetStatusFlags((I2C0)) & kI2C_IntPendingFlag)))	;
//
//	if(status & kI2C_ReceiveNakFlag)
//	{
//	return false;
//	}
//
//	I2C_MasterWriteBlocking(I2C0, &reg, 1, kI2C_TransferNoStopFlag);
//
//	/* Sends a start and a slave address. */
//	I2C_MasterRepeatedStart(I2C0, addr, kI2C_Read);
////	/* Waits for the sent out address. */
//	while(!((status = I2C_MasterGetStatusFlags((I2C0)) & kI2C_IntPendingFlag)))	;
//
//	if(status & kI2C_ReceiveNakFlag)
//	{
//	return false;
//	}
//
//	I2C_MasterReadBlocking(I2C0, data, len, kI2C_TransferRepeatedStartFlag);

	return true;
}







