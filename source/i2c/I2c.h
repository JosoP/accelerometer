/*
 * I2c.h
 *
 *  Created on: Nov 5, 2018
 *      Author: josop
 */

#ifndef I2C_I2C_H_
#define I2C_I2C_H_

#include <fsl_i2c.h>

class I2c {
private:
	i2c_master_config_t m_i2cMasterConfig;
	status_t 			m_lastOpStatus;

public:
	I2c();
	virtual ~I2c();

	void printLastStatus();

	bool write(uint8_t address, uint8_t* pData, size_t length);
	bool read(uint8_t address, uint8_t* pData, size_t length);
	bool readRegs(uint8_t addr, uint8_t reg, uint8_t * data, size_t length);
};

#endif /* I2C_I2C_H_ */
