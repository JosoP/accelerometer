/*
 * PIT.cpp
 *
 *  Created on: Nov 19, 2018
 *      Author: josop
 */

#include <PIT/pit.h>
#include "fsl_debug_console.h"
#include "fsl_pit.h"


#if defined(__cplusplus)
extern "C" {
#endif

void (*callbackFunctionPIT) (void) = nullptr;			// pointer to callback function

void PIT_IRQHandler(void){
	PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag );
	//PRINTF("PIT_IRQHandler");
	if(callbackFunctionPIT != nullptr)
		callbackFunctionPIT();
}

#if defined(__cplusplus)
}
#endif


pit::pit(uint64_t timeUs) {
	/* Structure of initialize PIT */
	pit_config_t pitConfig;

	PIT_GetDefaultConfig(&pitConfig);
	/* Init pit module */
	PIT_Init(PIT, &pitConfig);

	this->setPeriod(timeUs);

	/* Enable timer interrupts for channel 0 */
	PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);

	/* Enable at the NVIC */
	EnableIRQ(PIT_IRQn);
}

pit::~pit() {
	// TODO Auto-generated destructor stub
}

void pit::setPeriod(uint64_t timeUs) {
	/* Set timer period for channel 0 */
	PIT_SetTimerPeriod(
			PIT,
			kPIT_Chnl_0,
			USEC_TO_COUNT(timeUs, CLOCK_GetFreq(BUS_CLK)));
}

void pit::registerHandler(void (*pCallback)(void)) {
	callbackFunctionPIT = pCallback;
}

void pit::start() {
	/* Start channel 0 */
	PRINTF("\r\nPIT: Starting channel No.0 ...\n\r");
	PIT_StartTimer(PIT, kPIT_Chnl_0);
}
