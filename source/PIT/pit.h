/*
 * PIT.h
 *
 *  Created on: Nov 19, 2018
 *      Author: josop
 */

#ifndef PIT_PIT_H_
#define PIT_PIT_H_

#include <stdint.h>

class pit {

public:
	pit(uint64_t timeUs);
	virtual ~pit();

	void registerHandler(void (*pCallback) (void));

	void setPeriod(uint64_t timeUs);
	void start();
};

#endif /* PIT_PIT_H_ */
