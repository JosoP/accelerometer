/* Copyright (c) 2010-2011 mbed.org, MIT License
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software
* and associated documentation files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MMA8451Q_H
#define MMA8451Q_H


#include "i2c/I2c.h"


/**
* MMA8451Q accelerometer example
*
* @code
* #include "mbed.h"
* #include "MMA8451Q.h"
* 
* #define MMA8451_I2C_ADDRESS (0x1d<<1)
* 
* int main(void) {
* 
* MMA8451Q acc(P_E25, P_E24, MMA8451_I2C_ADDRESS);
* PwmOut rled(LED_RED);
* PwmOut gled(LED_GREEN);
* PwmOut bled(LED_BLUE);
* 
*     while (true) {       
*         rled = 1.0 - abs(acc.getAccX());
*         gled = 1.0 - abs(acc.getAccY());
*         bled = 1.0 - abs(acc.getAccZ());
*         wait(0.1);
*     }
* }
* @endcode
*/



class MMA8451Q
{
public:
	enum INT_SOURCE{
		INT_ASLP = 1<<7,
		INT_FIFO = 1<<6,
		INT_TRANS = 1<<5,
		INT_LNDPRT = 1<<4,
		INT_PULSE = 1<<3,
		INT_FF_MT = 1<<2,
		INT_DRDY = 1<<0
	};

	enum INT_POLARITY{
		IPOL_HIGH,
		IPOL_LOW
	};

	enum INT_PIN{
		INT_PIN1,
		INT_PIN2
	};

	enum PULSE_DIR{
		DIR_RIGHT,
		DIR_LEFT,
		DIR_FRONT,
		DIR_BACK,
		DIR_UP,
		DIR_DOWN,
		DIR_ERROR
	};


private:
	I2c m_i2c;
	int m_addr;


public:
  /**
  * MMA8451Q constructor
  *
  * @param sda SDA pin
  * @param sdl SCL pin
  * @param addr addr of the I2C peripheral
  */
  MMA8451Q(int addr);

  /**
  * MMA8451Q destructor
  */
  ~MMA8451Q();

  /**
   * Get the value of the WHO_AM_I register
   *
   * @returns WHO_AM_I value
   */
  bool isConnected();

  float getAccX();
  float getAccY();
  float getAccZ();
  void getAccAllAxis(float * res);

  void enableTabDetection();
  void enableInterrupt(INT_SOURCE source, INT_POLARITY polarity, INT_PIN pin);
  void registerHandlerInt1(void (*pCallback) (void));
  uint8_t getIntSource();
  PULSE_DIR getPulseDirection();

private:
  void readMoreRegs(uint8_t addr, uint8_t * data, int len);
  uint8_t readReg(uint8_t reg);
  void writeReg(uint8_t reg, uint8_t data);

  int16_t getAccAxis(uint8_t addr);
  void reset();
  void activate(bool activate);

};

#endif
