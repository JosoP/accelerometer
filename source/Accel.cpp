/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @author	JosoP
 * @file    Accel.cpp
 * @brief   Application entry point.
 */
#include "MMA8451Q/MMA8451Q.h"
#include "PIT/pit.h"
#include "power/powerManagement.h"

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */
#define ACCEL_I2C_ADDRESS 0x1D

#define F_C 		(float)5
#define DELTA_T		(float)0.1
#define PI			(float)3.14159265359


void printAxes(void);
void accelInt1Handler(void);

MMA8451Q* pAccel = nullptr;
static const float alpha = (2 * PI * DELTA_T * F_C)/(2 * PI * DELTA_T * F_C + 1);
/*
 * @brief   Application entry point.
 */
int main(void) {

  	/* Init board hardware. */
  	BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    PRINTF("Hello World\n\r");

    pAccel = new MMA8451Q(ACCEL_I2C_ADDRESS);
    pit timer = pit(int(DELTA_T * 1000000));



    pAccel->registerHandlerInt1(accelInt1Handler);
    pAccel->enableTabDetection();


	if(pAccel->isConnected()){
		PRINTF("Accel connected OK.\n\r");
	} else {
		PRINTF("ERROR - Accel not connected.\n\r");
	}


    timer.registerHandler(printAxes);
    timer.start();

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
//    	GPIO_WritePinOutput(
//    			BOARD_INITPINS_LED_BLUE_GPIO,
//				BOARD_INITPINS_LED_BLUE_PIN,
//				0);
    	//LED_BLUE_ON();
    	i++;
    	PRINTF("Going to sleep(%d)...\n\r", i);
    	PowerManagement::deepsleep();			// timer can't woke up MCU from sleep
    	//PowerManagement::sleep();				// timer can woke up MCU from sleep
    }
    return 0 ;
}

float lowFilter(float rawValue, float previousRaw){
	return ((1 - alpha) * previousRaw + alpha * rawValue);
}

void printAxes(void) {
	static float prevLowX, prevLowY, prevLowZ = 0;
	float loX, loY, loZ;
	float x, y, z;

	x = pAccel->getAccX();
	y = pAccel->getAccY();
	z = pAccel->getAccZ();

	loX = lowFilter(x, prevLowX);
	loY = lowFilter(y, prevLowY);
	loZ = lowFilter(z, prevLowZ);

	x = x - loX;
	y = y - loY;
	z = z - loZ;

	//PRINTF("Reading of accel: x = %.4f, y = %.4f, z = %.4f\r\n", x, y, z);
	PRINTF("%.4f %.4f %.4f\n\r", x, y, z);

	prevLowX = loX;
	prevLowY = loY;
	prevLowZ = loZ;
}

void accelInt1Handler(void){
	uint8_t src;

	src = pAccel->getIntSource();

	if(src & MMA8451Q::INT_PULSE){
		switch (pAccel->getPulseDirection()) {
			case MMA8451Q::DIR_RIGHT:
				PRINTF("Tap on right side...\n\r");
				break;
			case MMA8451Q::DIR_LEFT:
				PRINTF("Tap on left side...\n\r");
				break;
			case MMA8451Q::DIR_FRONT:
				PRINTF("Tap in front side...\n\r");
				break;
			case MMA8451Q::DIR_BACK:
				PRINTF("Tap in back side...\n\r");
				break;
			case MMA8451Q::DIR_UP:
				PRINTF("Tap on top side...\n\r");
				break;
			case MMA8451Q::DIR_DOWN:
				PRINTF("Tap on bottom side...\n\r");
				break;
			case MMA8451Q::DIR_ERROR:
				PRINTF("Some tap error...\n\r");
				break;
			default:
				break;
		}
	}
}




