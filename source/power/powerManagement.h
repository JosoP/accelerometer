/*
 * powerManagement.h
 *
 *  Created on: Jan 4, 2019
 *      \author josop
 */

#ifndef POWER_POWERMANAGEMENT_H_
#define POWER_POWERMANAGEMENT_H_

class PowerManagement{
public:

	static void sleep(void);
	static void deepsleep(void);
};

#endif /* POWER_POWERMANAGEMENT_H_ */
